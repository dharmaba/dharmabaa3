import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DharmabaComponent } from './dharmaba.component';

describe('DharmabaComponent', () => {
  let component: DharmabaComponent;
  let fixture: ComponentFixture<DharmabaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DharmabaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DharmabaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
