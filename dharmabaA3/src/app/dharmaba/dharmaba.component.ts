import { Component, OnInit } from '@angular/core';
import { MYBOOKS }  from "../../assets/data/myBooks";

@Component({
  selector: 'app-dharmaba',
  templateUrl: './dharmaba.component.html',
  styleUrls: ['./dharmaba.component.css']
})

export class DharmabaComponent implements OnInit {

  myBooks = MYBOOKS;
  currentBook = MYBOOKS[0];

  constructor() { }

  buttonClick(index) {
    this.currentBook = MYBOOKS[index];
  }

  ngOnInit() { }

}
