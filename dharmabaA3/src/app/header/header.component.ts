import { Component, OnInit } from '@angular/core';
import { Dharmaba } from '../dharmaba';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  user : Dharmaba = {
    id: 991474455,
    name: "Kasun Dharmabandhu",
    loginname: "dharmaba",
    campus: "Oakville",
    assignmenttitle: "Mobile Web Dev Assignment 03"
  }

  constructor() { }

  ngOnInit() {
  }

}
