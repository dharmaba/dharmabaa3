import { Component, OnInit } from '@angular/core';
import { Dharmaba } from '../dharmaba';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent implements OnInit {

  user : Dharmaba = {
    id: 991474455,
    name: "Kasun Dharmabandhu",
    loginname: "dharmaba",
    campus: "Oakville",
    assignmenttitle: "Mobile Web Dev Assignment 03"
  }

  constructor() { }

  ngOnInit() {
  }

}
