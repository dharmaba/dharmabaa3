import { Book } from '../../app/book';

export const MYBOOKS: Book [] = [
    { name:"Neuromancer", author:"William Gibson", year:1984, genre:"Science Fiction, Cyberpunk", image:"0.jpg" },
    { name:"Foundation", author:"Isaac Asimov", year:1951, genre:"Science Fiction, Political Drama", image:"1.jpg" },
    { name:"Do Androids Dream of Electric Sheep?", author:"Philip K. Dick", year:1968, genre:"Science Fiction, Philosophical Fiction", image:"2.png" },
    { name:"I, Robot", author:"Isaac Asimov", year:1950, genre:"Science Fiction", image:"3.jpg" }
];